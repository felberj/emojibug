# Description
When I have a commitmessage like ':muslce: added install instructions for icons', only the emoji ':muslce:' gets displayed.

It is normal under https://gitlab.com/felberj/emojibug (activity tab)

# Demo
https://gitlab.com/felberj/emojibug/commits/master  
https://gitlab.com/felberj/emojibug/tree/master
